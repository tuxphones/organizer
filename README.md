# Organizer

A simple, reference Gtk 3 / libhandy 1.x template based on [this tutorial by TuxPhones](https://tuxphones.com/tutorial-developing-responsive-linux-smartphone-apps-gnome-builder-gtk-libhandy-gtk-part-1/).

While the application itself should be a good starting point for new applications, it is worth remembering that the quickly evolving GNOME guidelines. For example, developing UI files manually seems to be considered a better practice than basing them on Glade or Builder's UI editor at the current level of maturity. Improvements are always welcome.

Also, the tutorial could be ported to Gtk 4 once the base becomes stable enough. 
